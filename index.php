<?php
include('includes/header.php');

if (isset($_GET['page']) && $_GET['page'] != "") {
  $file = 'includes/' . $_GET['page'] . ".inc.php";
  include($file);
} else {
  include('includes/home.php');
}
